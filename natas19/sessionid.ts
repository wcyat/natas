// author: chatgpt
export function hex2bin(text: string) {
    const hexString = text.match(/.{1,2}/g).map(byte => parseInt(byte, 16));
    const byteArray = new Uint8Array(hexString);
    const decoder = new TextDecoder();
    const decodedSessionID = decoder.decode(byteArray);
    return decodedSessionID;
}

export function bin2hex(text: string) {
    const encoder = new TextEncoder();
    const byteArray = encoder.encode(text);
    const hexString = byteArray.reduce((str, byte) => str + byte.toString(16).padStart(2, '0'), '');
    return hexString;
}

