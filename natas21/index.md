# Natas 21

There's nothing interesting in the first site (no way to set the cookie).

However, the collocated site seems to have a vulnerability that we can set whatever key
we want in the cookie in our request.

After adding `admin=1` and copying the cookie to the first site, we could get the password.
