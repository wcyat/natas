# Natas 16

## Analyze

From the source code, we can see that the input is sanitized, so we can't use
`"'[|&`. However, `$` and `()` are allowed. This means we can get some information using

```txt
cut -c $index /etc/natas_webpass/natas17
```

And check for the common character in the output.

We can also do `grep $char /etc/natas_webpass/natas17` to see if the password
matches a set of characters. (would return no words if matched, all words if unmatched).

## Password pattern

Using [bruteforce_insensitive.js](./bruteforce_insensitive.js),

We can find that the password is (case-insensitive, and numbers replaced with 0s) below:

```txt
xkeuche0sbnkbvh0ru0ksib0uulmi0sd
```

## Get the whole password

Using the above pattern, we can find the actual password using [bruteforce.ts](./bruteforce.ts).

As grep would match if a character is in a middle or anywhere, we need to provide the first
character to match (there is only one `x`, so we don't have to use two or more).

We can try `x` or `X`.

`x` doesn't match, so we can use `X` to find the password.

The password is:

```txt
XkEuChE0SbnKBvH1RU7ksIb9uuLmI7sd
```

which matches with above
