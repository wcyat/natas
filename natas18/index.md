# Natas 18

Achievement: I did not read any hints or answers!

## Analyze

From the source code, it seems impossible that we can be an admin by logging in,
no matter what password we try, as `isValidAdminLogin` always returns 0.

However, we could spot a vulnerability in the first php line.

```php
$maxid = 640; // 640 should be enough for everyone
```

There are too few session ids that we can try each of the ids and steal the admin privileges.

## Script

By running [steal_session.ts](./steal_session.ts), we successfully got the password in 37 seconds (the session id with admin privileges is `119`).
