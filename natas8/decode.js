// hextobin (reverse of bin2hex)
const hexString = "3d3d516343746d4d6d6c315669563362";
const buffer = Buffer.from(hexString, 'hex');
let s = buffer.toString('utf8');
// reverse the string
s = s.split("").reverse().join("");
// decode base64
s = atob(s);
console.log(s);
